# WordPress Post Tabs Base Skin

Skin base para el plugin WordPress Post Tabs para el tema GAIA Camp.


## Dependencias
| NOMBRE | VERSION | WP | WEB |
| :--- | :---: | :---: | :---: |
| GAIA Camp theme | 1.0.0 | - | -
| WordPress Post Tabs | 1.6.3 | [WordPress](https://es.wordpress.org/plugins/wordpress-post-tabs/) | [Web](http://tabbervilla.com/wordpress-post-tabs/)

## Instalación
* Instalación:
	1. Descarga e instala el plugin _WordPress Post Tabs_.
	2. Crea una carpeta llamada _base_ en el directorio del plugin ```/wp-content/plugins/wordpress-post-tabs/css/styles/``` y copia el contenido.
	3. Selecciona el nuevo skin desde el dashboard de Wordpress: ```Ajustes > WP Post Tabs > Skin/Style```
	4. Es necesario añadir en cabecera al siguiente código:
		```html
		<?php
			$title_background_color = '';
			if(get_post_meta($id, "qode_page-title-background-color", true) != ""){
			 $title_background_color = get_post_meta($id, "qode_page-title-background-color", true);
			}else{
				$title_background_color = $qode_options_elision['title_background_color'];
			}
		?>
		<style type="text/css">
		.wordpress-post-tabs-skin-base .ui-tabs .ui-tabs-nav li.ui-state-active a,
		.wordpress-post-tabs-skin-base .ui-tabs .ui-tabs-nav li.ui-state-disabled a,
		.wordpress-post-tabs-skin-base .ui-tabs .ui-tabs-nav li.ui-state-processing a {
			background-color: <?= $title_background_color; ?>;
		}
		.wordpress-post-tabs-skin-base .wpts_ext a {
			color: <?= $title_background_color; ?> !important;
		}
		.wordpress-post-tabs-skin-base a.wpts-mover {
			color: <?= $title_background_color; ?>;
		}
		</style>
		```